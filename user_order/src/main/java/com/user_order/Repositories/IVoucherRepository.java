package com.user_order.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.user_order.Model.Voucher;

public interface IVoucherRepository extends JpaRepository<Voucher, Integer> {
    
}
