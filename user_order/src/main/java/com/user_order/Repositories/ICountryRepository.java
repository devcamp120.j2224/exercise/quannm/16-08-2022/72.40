package com.user_order.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.user_order.Model.Country;

public interface ICountryRepository extends JpaRepository<Country , Integer>{
    
}
