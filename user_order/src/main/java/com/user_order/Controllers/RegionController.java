package com.user_order.Controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.Model.Country;
import com.user_order.Model.Region;
import com.user_order.Repositories.ICountryRepository;
import com.user_order.Repositories.IRegionRepository;

@RestController
public class RegionController {
	@Autowired
	private IRegionRepository regionRepository;
	
	@Autowired
	private ICountryRepository countryRepository;
	
	@CrossOrigin
	@PostMapping("/regions/{countryId}")
	public ResponseEntity<Object> createRegion(@PathVariable("countryId") int countryId, @RequestBody Region cRegion) {
		try {
			Optional<Country> countryData = countryRepository.findById(countryId);
			if (countryData.isPresent()) {
			Region newRole = new Region();
			newRole.setRegionName(cRegion.getRegionName());
			newRole.setRegionCode(cRegion.getRegionCode());
			newRole.setCountry(cRegion.getCountry());
			
			Country _country = countryData.get();
			newRole.setCountry(_country);
			newRole.setCountryName(_country.getCountryName());
			
			Region savedRole = regionRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Region: "+e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/regions/{regionId}")
	public ResponseEntity<Object> updateRegion(@PathVariable("regionId") int regionId, @RequestBody Region cRegion) {
		Optional<Region> regionData = regionRepository.findById(regionId);
		if (regionData.isPresent()) {
			Region newRegion = regionData.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			Region savedRegion = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRegion, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/regions/{regionId}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable int regionId) {
		try {
			regionRepository.deleteById(regionId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/regions/{regionId}")
	public Region getRegionById(@PathVariable int regionId) {
		if (regionRepository.findById(regionId).isPresent())
			return regionRepository.findById(regionId).get();
		else
			return null;
	}


	@CrossOrigin
	@GetMapping("/regions")
	public List<Region> getAllRegion() {
		return regionRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions")
    public List <Region> getRegionsByCountryId(@PathVariable(value = "countryId") int countryId) {
        return regionRepository.findByCountryId(countryId);
    }
	
	@CrossOrigin
	@GetMapping("/countries/regions")
    public List <Region> countRegionByCountryCode(@RequestParam(value = "countryCode") String countryCode) {
        return regionRepository.findByCountryCountryCode(countryCode);
    }

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions/{regionId}")
    public Optional<Region> getRegionByRegionAndCountry(@PathVariable(value = "countryId") int countryId,@PathVariable(value = "regionId") int regionId) {
        return regionRepository.findByIdAndCountryId(regionId,countryId);
    }

	@CrossOrigin
	@GetMapping("/countries/{countryId}/count")
    public int countRegionByCountryId(@PathVariable(value = "countryId") int countryId) {
        return regionRepository.findByCountryId(countryId).size();
    }
	
	@CrossOrigin
	@GetMapping("/regions/check/{regionId}")
	public boolean checkRegionById(@PathVariable int regionId) {
		return regionRepository.existsById(regionId);
	}		
}
