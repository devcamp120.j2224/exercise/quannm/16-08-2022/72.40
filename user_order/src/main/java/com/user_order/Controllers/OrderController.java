package com.user_order.Controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.Model.Order;
import com.user_order.Model.User;
import com.user_order.Repositories.IOrderRepository;
import com.user_order.Repositories.IUserRepository;
import com.user_order.Services.UserService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderController {
    @Autowired
    IOrderRepository orderRepository;

    @Autowired
    IUserRepository userRepository;


    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrder(){
        return new ResponseEntity<>(orderRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/orders/{userId}")
    public ResponseEntity<List<Order>> getOrderByUserId(@PathVariable ("userId") int userId){
        try {
            UserService userService = new UserService();
            List<Order> orderData = userService.getListOrdersByUserId(userRepository, userId);
            if(orderData.size() != 0)
                return new ResponseEntity<>(orderData, HttpStatus.OK);
            else 
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/orders/{userId}")
    public ResponseEntity<Object> createOrder(@Valid @RequestBody Order order, @PathVariable("userId") int userId){
        try {
            Optional<User> userData = userRepository.findById(userId);
            if(userData.isPresent()){
                Order newOrder = new Order();
                newOrder.setId(order.getId());
                newOrder.setKichCo(order.getKichCo());
                newOrder.setLoaiPizza(order.getLoaiPizza());
                newOrder.setOrderCode(order.getOrderCode());
                newOrder.setThanhTien(order.getThanhTien());
                newOrder.setVoucher(order.getVoucher());

                User _user = userData.get();
                newOrder.setUser(_user);
                
                Order saveOrder = orderRepository.save(newOrder);
                return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
            }
        } catch (Exception e){
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Order: "+e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } 

    @PutMapping("/orders/{orderId}")
    public ResponseEntity<Object> updateOrder (@Valid @RequestBody Order order, @PathVariable ("orderId") int orderId){
        try {
            Optional<Order> orderData = orderRepository.findById(orderId);
            if(orderData.isPresent()){
                Order order1 = orderData.get();
                Order newOrder = new Order();
                newOrder.setId(order1.getId());
                newOrder.setKichCo(order.getKichCo());
                newOrder.setLoaiPizza(order.getLoaiPizza());
                newOrder.setOrderCode(order1.getOrderCode());
                newOrder.setThanhTien(order.getThanhTien());
                newOrder.setVoucher(order.getVoucher());
                newOrder.setUser(order.getUser());

                Order saved = orderRepository.save(newOrder);
                return new ResponseEntity<>(saved, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

            }
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/orders/{orderId}")
    public ResponseEntity<Object> deleteUser(@PathVariable ("orderId") int orderId){
        if (orderRepository.findById(orderId).isPresent()){
            orderRepository.deleteById(orderId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
