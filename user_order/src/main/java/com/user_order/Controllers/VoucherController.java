package com.user_order.Controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.Model.Voucher;
import com.user_order.Repositories.IVoucherRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class VoucherController {
    
    @Autowired
    IVoucherRepository pVoucherRepository;

    @GetMapping("/voucher")
    public List<Voucher> getAllVoucher(){
            
        return pVoucherRepository.findAll();
    
    }

    @GetMapping("/voucher/{id}")
    public ResponseEntity<Voucher> getVoucherById(@PathVariable("id") int id){

        try {
            Optional<Voucher> voucherData = pVoucherRepository.findById(id);
            if(voucherData.isPresent()){
                return new ResponseEntity<>(voucherData.get(),HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/voucher")
    public ResponseEntity<Object> createVoucher(@RequestBody Voucher voucher){
        try {
            Optional<Voucher> voucherData = pVoucherRepository.findById(voucher.getId());
            if(voucherData.isPresent()){
                return ResponseEntity.unprocessableEntity().body("Voucher already exsit");
            }else{
                Voucher newVoucher = new Voucher();
                newVoucher.setMaGiamGia(voucher.getMaGiamGia());
                newVoucher.setPhamTramGiamGia(voucher.getPhamTramGiamGia());
                newVoucher.setNgayTao(new Date());
                newVoucher.setGhiChu(voucher.getGhiChu());
    
                Voucher saved = pVoucherRepository.save(newVoucher);
                return new ResponseEntity<>(saved,HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to create Voucher");
        }
    }

    @PutMapping("/voucher/{id}")
    public ResponseEntity<Object> updateVoucher(@PathVariable("id") int id , @RequestBody Voucher voucher){
        try {
            Optional<Voucher> voucherData = pVoucherRepository.findById(id);
            if(voucherData.isPresent()){
                Voucher newVoucher = voucherData.get();
                newVoucher.setMaGiamGia(voucher.getMaGiamGia());
                newVoucher.setPhamTramGiamGia(voucher.getPhamTramGiamGia());
                newVoucher.setNgayCapNhat(new Date());
                newVoucher.setNgayTao(voucherData.get().getNgayTao());
                newVoucher.setGhiChu(voucher.getGhiChu());
    
                Voucher savedVoucher = pVoucherRepository.save(newVoucher);
                return new ResponseEntity<>(savedVoucher,HttpStatus.OK);
            }else{
                return ResponseEntity.unprocessableEntity().body("Failed to update Voucher");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/voucher/{id}")
    public ResponseEntity<String> deleteVoucherById(@PathVariable("id") int id){
        try {
            pVoucherRepository.deleteById(id);
            return new ResponseEntity<>("Xóa thành công",HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>("Xóa không thành công",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
