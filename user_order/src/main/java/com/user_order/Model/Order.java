package com.user_order.Model;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "don_hang")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    
    @Column(name = "order_code", unique = true)
    private String orderCode;
    
    @Column(name = "thanh_tien")
    private String thanhTien;

    @Column(name = "kich_co")
    private char kichCo;

    @Column(name = "loai_pizza")
    private String loaiPizza;
    
    @Column (name = "ma_voucher")
    private String voucher;
    
    @ManyToOne
    @JsonIgnore
    private User user;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getOrderCode() {
        return orderCode;
    }
    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
    public String getThanhTien() {
        return thanhTien;
    }
    public void setThanhTien(String thanhTien) {
        this.thanhTien = thanhTien;
    }
    public char getKichCo() {
        return kichCo;
    }
    public void setKichCo(char kichCo) {
        this.kichCo = kichCo;
    }
    public String getLoaiPizza() {
        return loaiPizza;
    }
    public Order(int id, String orderCode, String thanhTien, char kichCo, String loaiPizza, String voucher, User user) {
        this.id = id;
        this.orderCode = orderCode;
        this.thanhTien = thanhTien;
        this.kichCo = kichCo;
        this.loaiPizza = loaiPizza;
        this.voucher = voucher;
        this.user = user;
    }
    public void setLoaiPizza(String loaiPizza) {
        this.loaiPizza = loaiPizza;
    }
    public String getVoucher() {
        return voucher;
    }
    public Order() {
    }
    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
