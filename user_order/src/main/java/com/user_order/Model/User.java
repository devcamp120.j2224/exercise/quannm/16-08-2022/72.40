package com.user_order.Model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "khach_hang")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @NotNull(message = "Địa chỉ không được trống")
    @Column(name = "dia_chi")
    private String diaChi;

    @NotNull(message = "Email không được trống")
    @Column(name = "email")
    private String email;

    @NotNull(message = "Hãy nhập họ tên")
    @Column(name = "ho_ten")
    private String hoTen;

    @Column(name = "so_dien_thoai", unique = true)
    private String soDienThoai;

    @OneToMany(targetEntity = Order.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private Set<Order> orders;

    public User() {
    }

    public User(int id, @NotNull(message = "Địa chỉ không được trống") String diaChi,
            @NotNull(message = "Email không được trống") String email,
            @NotNull(message = "Hãy nhập họ tên") String hoTen, String soDienThoai, Set<Order> orders) {
        this.id = id;
        this.diaChi = diaChi;
        this.email = email;
        this.hoTen = hoTen;
        this.soDienThoai = soDienThoai;
        this.orders = orders;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }


}
