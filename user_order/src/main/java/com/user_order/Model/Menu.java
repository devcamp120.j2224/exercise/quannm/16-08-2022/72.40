package com.user_order.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "don_gia")
    private long donGia;

    @Column(name = "duong_kinh")
    private String duongKinh;

    @Column(name = "salad")
    private int salad;

    @Column(name = "size")
    private char size;

    public Menu(int id, long donGia, String duongKinh, int salad, char size, int soLuongNuoc, int suon) {
        this.id = id;
        this.donGia = donGia;
        this.duongKinh = duongKinh;
        this.salad = salad;
        this.size = size;
        this.soLuongNuoc = soLuongNuoc;
        this.suon = suon;
    }

    @Column(name = "so_luong_nuoc")
    private int soLuongNuoc;

    public Menu() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getDonGia() {
        return donGia;
    }

    public void setDonGia(long donGia) {
        this.donGia = donGia;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public char getSize() {
        return size;
    }

    public void setSize(char size) {
        this.size = size;
    }

    public int getSoLuongNuoc() {
        return soLuongNuoc;
    }

    public void setSoLuongNuoc(int soLuongNuoc) {
        this.soLuongNuoc = soLuongNuoc;
    }

    public int getSuon() {
        return suon;
    }

    public void setSuon(int suon) {
        this.suon = suon;
    }

    @Column(name = "suon")
    private int suon;
}
