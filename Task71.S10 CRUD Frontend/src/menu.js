let menuTable = $('#menu-table').DataTable({
  columns: [
    { data: 'size' },
    { data: 'duongKinh' },
    { data: 'suon' },
    { data: 'salad' },
    { data: 'soLuongNuoc' },
    { data: 'donGia' },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
});

function loadMenuToTable(paramMenu) {
  menuTable.clear();
  menuTable.rows.add(paramMenu);
  menuTable.draw();
}

function getMenuFromDb() {
  $.get(`http://localhost:8080/menus`, loadMenuToTable);
}
getMenuFromDb();

let gMenuId = 0;
let menu = {
  newMenu: {
    size: '',
    duongKinh: '',
    suon: '',
    salad: '',
    soLuongNuoc: '',
    donGia: '',
  },
  onCreateMenuClick() {
    gMenuId = 0;
    this.newMenu = {
      size: $('#input-pizza-size').val().trim(),
      duongKinh: $('#input-pizza-diameter').val().trim(),
      suon: $('#input-meat').val().trim(),
      salad: $('#input-salad').val().trim(),
      soLuongNuoc: $('#input-drink').val().trim(),
      donGia: $('#input-price').val().trim(),
    };
    if (validateMenu(this.newMenu)) {
      $.ajax({
        url: `http://localhost:8080/menus`,
        method: 'POST',
        data: JSON.stringify(this.newMenu),
        contentType: 'application/json',
        success: () => {
          alert('Menu created successfully');
          resetMenu();
          getMenuFromDb();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onUpdateMenuClick() {
    let vSelectRow = $(this).parents('tr');
    let vSelectedData = menuTable.row(vSelectRow).data();
    console.log(vSelectedData);
    gMenuId = vSelectedData.id;
    $.get(`http://localhost:8080/menus/${gMenuId}`, loadMenuToInput);
  },
  onSaveMenuClick() {
    this.newMenu = {
      size: $('#input-pizza-size').val().trim(),
      duongKinh: $('#input-pizza-diameter').val().trim(),
      suon: $('#input-meat').val().trim(),
      salad: $('#input-salad').val().trim(),
      soLuongNuoc: $('#input-drink').val().trim(),
      donGia: $('#input-price').val().trim(),
    };
    if (validateMenu(this.newMenu)) {
      $.ajax({
        url: `http://localhost:8080/menus/${gMenuId}`,
        method: 'PUT',
        data: JSON.stringify(this.newMenu),
        contentType: 'application/json',
        success: () => {
          alert('Menu updated successfully');
          resetMenu();
          getMenuFromDb();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onDeleteIconClick() {
    $('#modal-delete-menu').modal('show');
    let vSelectRow = $(this).parents('tr');
    let vSelectedData = menuTable.row(vSelectRow).data();
    gMenuId = vSelectedData.id;
  },
  onDeleteAllMenuClick() {
    $('#modal-delete-menu').modal('show');
    gMenuId = 0;
  },
  onConfirmDeleteClick() {
    if (gMenuId === 0) {
      $.ajax({
        url: `http://localhost:8080/menus`,
        method: 'DELETE',
        success: () => {
          alert('All menu were successfully deleted');
          $('#modal-delete-menu').modal('hide');
          getMenuFromDb();
        },
        error: (err) => alert(err.responseText),
      });
    } else {
      $.ajax({
        url: `http://localhost:8080/menus/${gMenuId}`,
        method: 'DELETE',
        success: () => {
          alert(`menu with id: ${gMenuId} was successfully deleted`);
          $('#modal-delete-menu').modal('hide');
          getMenuFromDb();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
};

$('#create-menu').click(menu.onCreateMenuClick);
$('#menu-table').on('click', '.fa-edit', menu.onUpdateMenuClick);
$('#menu-table').on('click', '.fa-trash', menu.onDeleteIconClick);
$('#update-menu').click(menu.onSaveMenuClick);
$('#delete-all-menu').click(menu.onDeleteAllMenuClick);
$('#delete-menu').click(menu.onConfirmDeleteClick);

function validateMenu(paramMenu) {
  let vResult = true;
  try {
    if (paramMenu.size == '') {
      vResult = false;
      throw 'Size không được để trống';
    }
    if (paramMenu.duongKinh == '') {
      vResult = false;
      throw 'Đường kính không được để trống';
    }
    if (paramMenu.suon == '') {
      vResult = false;
      throw 'Số lượng thịt không được để trống';
    }
    if (paramMenu.salad == '') {
      vResult = false;
      throw 'Salad không được để trống';
    }
    if (paramMenu.soLuongNuoc == '') {
      vResult = false;
      throw 'Số lượng nước không được để trống';
    }
    if (paramMenu.donGia == '') {
      vResult = false;
      throw 'Giá không được để trống';
    }
  } catch (e) {
    alert(e);
  }
  return vResult;
}

function loadMenuToInput(paramMenu) {
  $('#input-pizza-size').val(paramMenu.size);
  $('#input-pizza-diameter').val(paramMenu.duongKinh);
  $('#input-meat').val(paramMenu.suon);
  $('#input-salad').val(paramMenu.salad);
  $('#input-drink').val(paramMenu.soLuongNuoc);
  $('#input-price').val(paramMenu.donGia);
}

function resetMenu() {
  $('#input-pizza-size').val('');
  $('#input-pizza-diameter').val('');
  $('#input-meat').val('');
  $('#input-salad').val('');
  $('#input-drink').val('');
  $('#input-price').val('');
}
